let boys = [];
let girls = [];
let fullData = {};

async function fetchData(year) {
	boys = [];
	girls = [];

	let res = await fetch(`data/yob${year}.txt`);
	let text = await res.text();

	let start = Date.now();

	let index = text.split("\n");
	index.forEach(entry => {
		entry = entry.split(",");
		entry[2] = parseInt(entry[2]);
		if (entry[1] === "M") boys.push(entry);
		else girls.push(entry);
	});

	console.log(
		`Loaded ${boys.length} boy names and ${girls.length} girl names in ${
			Date.now() - start
		}ms`
	);
}

function getName(scrollToTop = false) {
	// Setup/filter by gender
	let list;
	if (app.gender === "Either") list = [...boys, ...girls];
	else if (app.gender === "Male") list = [...boys];
	else list = [...girls];

	// Filter by use count
	list = list.filter(
		([_, _2, count]) => count >= app.min && count <= app.max
	);

	console.log(`${list.length} potential names`);

	// Random sort + Set amount
	list = _.sampleSize(list, app.amount);

	app.names = list;

	if (scrollToTop) window.scrollTo(0, 0);
}

fetchData(2021);

const MAX_YEAR = 2019;
const MIN_YEAR = 1880;
let yearList = new Array(MAX_YEAR - MIN_YEAR + 1)
	.fill(0)
	.map((_, i) => MAX_YEAR - i);

function bookmark(...stuff) {
	if (bookmarked(...stuff))
		app.bookmarks = app.bookmarks.filter(bm => {
			return (
				bm[0] !== stuff[0] ||
				bm[1] !== stuff[1] ||
				bm[2] !== stuff[2] ||
				bm[3] !== stuff[3]
			);
		});
	else app.bookmarks.push(stuff);

	localStorage.setItem("names-bookmarks", JSON.stringify(app.bookmarks));
}

function bookmarked(name, gen, ct, year) {
	return app.bookmarks.some(bm => {
		return (
			bm[0] === name && bm[1] === gen && bm[2] === ct && bm[3] === year
		);
	});
}

async function downloadFull() {
	app.fullDownloadState = "Downloading...";

	let full = await fetch("data/full.txt");
	full = await full.text();

	app.fullDownloadState = "Parsing...";

	await sleep(1);

	full = full.split("\n");
	full.forEach(entry => {
		entry = entry.split(" ");
		// app.fullDownloadState = `Parsing year ${entry[0]}...`
		// console.log(entry);
		fullData[entry[0]] = {
			men: entry[1].split("&").map(e => e.split(",")),
			women: entry[2].split("&").map(e => e.split(",")),
		};
	});

	app.fullDownloaded = true;
}

async function searchFull() {
	app.thinking = true;

	await sleep(1);
	
	let query = app.fullSearch.toLowerCase();

	let results = [];
	for (const year in fullData) {
		if (app.fullGender === "Either" || app.fullGender === "Male")
			fullData[year].men.forEach(man => {
				if (app.fullFilter === "eq") {
					if (man[0].toLowerCase() === query) results.push([man[0], "M", man[1], year]);
				} else if (man[0].toLowerCase()[app.fullFilter](query))
					results.push([man[0], "M", man[1], year]);
			});
		if (app.fullGender === "Either" || app.fullGender === "Female")
			fullData[year].women.forEach(woman => {
				if (app.fullFilter === "eq") {
					if (woman[0].toLowerCase() === query) results.push([woman[0], "F", woman[1], year]);
				} else if (woman[0].toLowerCase()[app.fullFilter](query))
					results.push([woman[0], "F", woman[1], year]);
			});
	}

	results = results.map(r => {
		r[2] = parseInt(r[2]);
		r[4] = parseInt(r[4]);
		return r;
	});

	app.fullResults = results;
	app.fullPeople = results.reduce((prev, curr) => prev + curr[2], 0);
	app.thinking = false;
}

const app = Vue.createApp({
	data() {
		return {
			min: 5,
			max: 100,
			gender: "Either",
			amount: 100,
			names: [],

			year: 2021,
			yearList,

			bookmarks: [],

			tab: "main",

			fullDownloaded: false,
			fullDownloadState: "",

			fullSearch: "",
			fullFilter: "includes",
			fullGender: "Either",
			fullResults: [],
			fullPeople: 0,
			powerMode: false,
			thinking: false,
		};
	},
	methods: {
		getName,
		bookmark,
		bookmarked,
		validateMin() {
			if (this.min < 5) this.min = 5;
		},
		validateMax() {
			if (this.min > this.max) this.max = this.min;
		},
		downloadFull,
		searchFull,
	},
	watch: {
		year(yr) {
			fetchData(yr);
		},
	},
}).mount("#app");

try {
	app.bookmarks = JSON.parse(localStorage.getItem("names-bookmarks")) ?? [];
} catch (e) {}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}
