let startYear = 1880;
let endYear = 2021;

let data = {};

for (let i = startYear; i <= endYear; i++) {
	let buffer = await Deno.readTextFile(`data/yob${i}.txt`);
	buffer = buffer.split("\r\n")

	let men = [];
	let women = [];

	buffer.forEach(entry => {
		entry = entry.split(",");
		if (entry[2]) entry[2] = parseInt(entry[2]);
		if (entry[1] === "M") men.push([entry[0], entry[2]])
		else if (entry[1] === "F") women.push([entry[0], entry[2]])
		else if (entry[0] !== "") console.error("WTF", entry);
	})

	men = men.map(entry => entry.join(",")).join("&");
	women = women.map(entry => entry.join(",")).join("&");

	data[i] = {men, women}
}

let finalBuffer = "";

for (const year in data) {
	finalBuffer += `\n${year} ${data[year].men} ${data[year].women}`
}

await Deno.writeTextFile("data/full.txt", finalBuffer.substring(1));

export {};