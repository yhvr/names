# uncommon baby names

a little page for bad baby names.

## how it works

i parse data from social security (see the "about" tab on the site) about how many babies each have a certain name in a set year. then i filter it to take only rare names, and then get a random sample of the names.

## setup

**you can try it in your browser [on my website](https://yhvr.me/names).**

1. clone the repository, and `cd` into it
2. (optional, only if you want search-ability) run `parseData.ts` with deno
3. set up a websever in this directory (you can't just navigate to the file in your browser, because the website uses `fetch`)